<%--
  Created by IntelliJ IDEA.
  User: Sven
  Date: 29/11/2017
  Time: 09:40
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<label for="min-orf-length" class="mt-1">Minimal open reading frame length:</label><br/>
<input id="min-orf-length" name="orf-length" class="form-control" type="number" placeholder="Length of ORF" required/>
<label class="mt-1">Start position:</label><br/>
<input name="start" class="form-control" type="number" placeholder="Length of ORF" value="0" required/>
<label class="mt-1">Stop position: (n characters counted from the END of the sequence)</label><br/>
<input name="stop" class="form-control" type="number" placeholder="Length of ORF" value="0" required/>

<c:set var="options" value="${['-3', '-2', '-1', '+1', '+2', '+3', '6-frame']}" scope="page"/>

<br/>
<p>Select the reading frames which need to be showed:</p><br/>
<c:forEach items="${options}" var="option">
    <label>
        <input type="checkbox" value="${option}" name="options"/>
        <span>${option}</span>
    </label><br/>
</c:forEach>