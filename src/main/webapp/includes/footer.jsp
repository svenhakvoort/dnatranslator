<%--
  Created by IntelliJ IDEA.
  User: Sven
  Date: 22/11/2017
  Time: 20:06
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<div class="container footer">
    <p><i class="fa fa-copyright" aria-hidden="true"></i> 2017: <a
            href="https://www.linkedin.com/in/sven-hakvoort-3199a0101/">Sven Hakvoort</a></p>
</div>

</body>
<%-- BOOTSTRAP Dependencies--%>
<script src="${pageContext.request.contextPath}/js/lib/jquery-3.2.1.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.3/umd/popper.min.js"
        integrity="sha384-vFJXuSJphROIrBnz7yo7oB41mKfc8JzQZiCq4NCceLEaO4IHwicKwpJf9c9IpFgh"
        crossorigin="anonymous"></script>
<script src="${pageContext.request.contextPath}/lib/bootstrap/js/bootstrap.min.js"></script>

<c:if test="${pageContext.request.servletPath == '/index.jsp'}">
    <script src="${pageContext.request.contextPath}/js/inputoptions.js"></script>
</c:if>

</html>
