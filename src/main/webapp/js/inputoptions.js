$(document).ready(function () {

    // When the form is submit make sure at least one checkbox is selected
    $("form").on("submit", function () {
        event.preventDefault();

        var checked = $("input[type=checkbox]:checked").length;

        if (checked === 0) {
            alert("You must check at least one checkbox.");
        } else {
            this.submit();
        }
    });

    // Change the displayed input option for the fasta sequence
    $("input[name=inputOption]").change(function () {

        $("input[name=inputOption]").each(function () {

            var value = $(this).val();
            var elementToChange = $('#input-' + value);
            if (elementToChange.hasClass("d-none")) {
                elementToChange.removeClass("d-none");
            } else {
                elementToChange.addClass("d-none");
            }
        });

        var value = $(this).val();
        var elementToChange = $('#input-' + value);
        $(elementToChange).insertBefore($("body").find("div.input-option").first());
    });

    // When 6-frame is selected disable all other checkboxes since these become obsolete when this happens
    // Revert to the original state when it is deselected
    $("input[type=checkbox]").change(function () {
        // If 6-frame option is selected
        if ($(this).val() === "6-frame") {
            // Save state of option
            var checked = $(this).prop("checked");
            // Disable or enable all the other checkboxes
            $("input[type=checkbox]").each(function () {
                if ($(this).val() !== "6-frame") {
                    if (checked) {
                        $(this).prop('disabled', true);
                    } else {
                        $(this).prop('disabled', false);
                    }
                }
            });
        }
    });
});