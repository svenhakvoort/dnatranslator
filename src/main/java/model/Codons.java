package model;

import java.util.HashMap;

/**
 * A class which contains a static hashmap in which all codons are saved with their corresponding aminoacid
 *
 * @author Sven Hakvoort [sven.hakvoort@gmail.com]
 * @version 1.0
 */
public class Codons {

    private static final HashMap<String, String> aminoAcids = new HashMap<String, String>() {
        {
            put("TTT", "F");
            put("TTC", "F");
            put("TTA", "L");
            put("TTG", "L");
            put("TCT", "S");
            put("TCC", "S");
            put("TCA", "S");
            put("TCG", "S");
            put("TAT", "Y");
            put("TAC", "Y");
            put("TAA", "*");
            put("TAG", "*");
            //            TAG end
            put("TGT", "C");
            put("TGC", "C");
            put("TGA", "*");
            //            TGA end
            put("TGG", "W");
            put("CTT", "L");
            put("CTC", "L");
            put("CTA", "L");
            put("CTG", "L");
            put("CCT", "P");
            put("CCC", "P");
            put("CCA", "P");
            put("CCG", "P");
            put("CAT", "H");
            put("CAC", "H");
            put("CAA", "Q");
            put("CAG", "Q");
            put("CGT", "R");
            put("CGC", "R");
            put("CGA", "R");
            put("CGG", "R");
            put("ATT", "I");
            put("ATC", "I");
            put("ATA", "I");
            put("ATG", "M");
            put("ACT", "T");
            put("ACC", "T");
            put("ACA", "T");
            put("ACG", "T");
            put("AAT", "N");
            put("AAC", "N");
            put("AAA", "K");
            put("AAG", "K");
            put("AGT", "S");
            put("AGC", "S");
            put("AGA", "R");
            put("AGG", "R");
            put("GTT", "V");
            put("GTC", "V");
            put("GTA", "V");
            put("GTG", "V");
            put("GCT", "A");
            put("GCC", "A");
            put("GCA", "A");
            put("GCG", "A");
            put("GAT", "D");
            put("GAC", "D");
            put("GAA", "E");
            put("GAG", "E");
            put("GGT", "G");
            put("GGC", "G");
            put("GGA", "G");
            put("GGG", "G");
        }
    };

    /**
     * Returns a specific amino acid based on the codon
     * @param codon A three character string of which to find the corresponding amino acid
     * @return a string for the amino acid
     */
    public static String getAminoAcid(String codon) {
        return aminoAcids.get(codon);
    }
}
