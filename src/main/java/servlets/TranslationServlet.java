package servlets;

import model.FormHandler;
import model.Sequence;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

@WebServlet(name = "TranslationServlet", urlPatterns = "/translation")
public class TranslationServlet extends HttpServlet {

    private String filePath;

    /**
     * Initializes the servlet, contains the upload folder location
     */
    public void init() {
        filePath = getServletContext().getInitParameter("file-upload");
    }

    /**
     * If data is posted to the servlet, process the data and return the result
     */
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        boolean isMultiPartForm = request.getContentType().toLowerCase().startsWith("multipart");

        response.setContentType("text/html");

        FormHandler handler = new FormHandler();
        handler.processFormData(isMultiPartForm, request, filePath);

        // If the processing was successful, show the results
        if (isSuccesfull(request, handler.getSequence())) {

            request.setAttribute("sequence", handler.getSequence());

            RequestDispatcher view = request.getRequestDispatcher("translation.jsp");

            saveSession(request.getSession(), handler.getSequence());

            view.forward(request, response);
        } else {
            RequestDispatcher dispatcher = request.getRequestDispatcher("index.jsp");
            dispatcher.forward(request, response);
        }
    }

    /**
     * The method being called if a user requests an item from the history and show this result to the user
     */
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setContentType("text/html");

        HttpSession session = request.getSession();

        // If there is a session history, else redirect to the homepage
        if (session.getAttribute("previous") != null) {

            List<Sequence> sequences = (List<Sequence>) session.getAttribute("previous");

            // Get the index in the list from the get parameter
            int historyIndex = Integer.parseInt(request.getParameter("history"));

            request.setAttribute("sequence", sequences.get(historyIndex));

            RequestDispatcher view = request.getRequestDispatcher("translation.jsp");

            view.forward(request, response);
        } else {
            response.sendRedirect("/");
        }
    }

    /**
     * Checks if the processing of the fasta succeeded, if not returns an error message
     */
    private boolean isSuccesfull(HttpServletRequest request, Sequence sequence) {
        boolean succes = true;

        request.setAttribute("title", "DNA Translator");

        if (!sequence.isValid() ) {
            request.setAttribute("failed", "The input file was not a valid FASTA file or DNA sequence!");
            succes = false;
        }

        return succes;
    }

    /**
     * Save the current object in the history, so it can be requested again
     *
     * @param session  A Http session object
     * @param sequence the sequence object to be saved in the session
     */
    private void saveSession(HttpSession session, Sequence sequence) {
        List<Sequence> sequences;
        // If session does not exist
        if (session.getAttribute("previous") == null) {
            sequences = new ArrayList<>();
        } else {
            // Request the session and cast it to the right type
            sequences = (List<Sequence>) session.getAttribute("previous");
        }

        // Add it at the top
        sequences.add(0, sequence);
        // If too much in history, remove the last one
        if (sequences.size() >= 30) {
            sequences.remove(sequences.size() - 1);
        }
        session.setAttribute("previous", sequences);
    }
}
